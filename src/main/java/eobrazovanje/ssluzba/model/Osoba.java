package eobrazovanje.ssluzba.model;

import java.util.Date;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Person */
@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public class Osoba extends BaseEntity {
    @NotBlank
    @Size(min = 3)
    private String ime;

    @NotBlank
    @Size(min = 3)
    private String prezime;

    @NotNull
    @Temporal(TemporalType.DATE) // Da mapira ovaj datum na datum u MySQL
    private Date datumRodjenja;

    @NotBlank private String mestoRodjenja;
}
