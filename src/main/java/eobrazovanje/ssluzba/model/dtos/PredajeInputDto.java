package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.Uloga;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PredajeInputDto {

    private Uloga uloga;
}
