package eobrazovanje.ssluzba.model.dtos;

import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UplataInputDto {
    private Long id;
    @NotNull private Double kolicina;
    @NotNull private UcenikOutputDto ucenik;
    @NotNull private Date vreme;
}
