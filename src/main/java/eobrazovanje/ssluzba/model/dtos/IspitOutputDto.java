package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.IspitniRok;
import eobrazovanje.ssluzba.model.enums.TipIspita;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IspitOutputDto {
    private Long id;
    @NotNull private TipIspita tipIspita;

    @NotNull private IspitniRok ispitniRok;

    @NotNull private Double maksimumBodova;

    @NotNull private PredmetWithoutPredajeDto predmet;

    List<PrijavaOutputDto> prijave;
}
