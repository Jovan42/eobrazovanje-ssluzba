package eobrazovanje.ssluzba.model.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PredmetInputDto {
    private Long id;
    @NotBlank private String naziv;

    @NotNull private Integer espb;
}
