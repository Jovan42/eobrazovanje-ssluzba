package eobrazovanje.ssluzba.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DokumentOutputDto {
    private Long id;
    private String naslov;
    private Long ucenik;
    private String path;
}
