package eobrazovanje.ssluzba.model.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IzmenaRacunaDto {
    private Double izmenaRacuna;
}
