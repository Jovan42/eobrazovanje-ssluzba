package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.TipStudija;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UcenikInputDto extends OsobaDto {
    @NotBlank private String brojIndeksa;
    @NotNull private TipStudija tipStudija;
    private String racun;
    private Double stanjeRacun;
    @NotEmpty private String studijskiProgram;
    private String username;
    private String password;
}
