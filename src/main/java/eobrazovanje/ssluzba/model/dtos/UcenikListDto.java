package eobrazovanje.ssluzba.model.dtos;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UcenikListDto {
    List<UcenikOutputDto> ucenici;
}
