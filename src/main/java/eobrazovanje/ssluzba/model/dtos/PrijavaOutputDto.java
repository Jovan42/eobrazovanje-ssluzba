package eobrazovanje.ssluzba.model.dtos;

import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrijavaOutputDto {
    @ManyToOne() private UcenikOutputDto ucenik;
    @ManyToOne() private Long ispit;

    private Double osvojeniBodovi;
}
