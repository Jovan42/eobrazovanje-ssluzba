package eobrazovanje.ssluzba.model.dtos;

import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ErrorMessage {
    private Date timestamp;
    private Integer status;
    private String error;
    private List<String> messages;

    public ErrorMessage(Integer status, String error, List<String> messages) {
        timestamp = new Date();
        this.status = status;
        this.error = error;
        this.messages = messages;
    }
}
