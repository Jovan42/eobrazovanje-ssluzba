package eobrazovanje.ssluzba.model.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PohadjaInputDto {
    private Long id;

    private Integer konacnaOcena;
}
