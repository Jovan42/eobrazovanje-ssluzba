package eobrazovanje.ssluzba.model.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PredmetWithoutPredajeDto {
    private Long id;
    @NotBlank private String naziv;

    @NotNull private Integer espb;
}
