package eobrazovanje.ssluzba.model.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class NastavnikOutputDto extends OsobaDto {
    private String username;
}
