package eobrazovanje.ssluzba.model.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OcenaPredmetaDto {
    private Integer ocena;
}
