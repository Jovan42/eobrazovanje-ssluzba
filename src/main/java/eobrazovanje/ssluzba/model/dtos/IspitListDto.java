package eobrazovanje.ssluzba.model.dtos;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IspitListDto {
    List<IspitOutputDto> ispiti;
}
