package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.TipStudija;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UcenikOutputDto extends OsobaDto {
    @NotBlank private String brojIndeksa;
    @NotNull private TipStudija tipStudija;

    private String racun;
    private Double stanjeRacun;
    private String username;
    private String password;
    @NotEmpty private String studijskiProgram;
    private List<UplataOutputDto> uplate;
}
