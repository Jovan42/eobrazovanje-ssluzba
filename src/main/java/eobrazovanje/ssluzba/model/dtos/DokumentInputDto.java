package eobrazovanje.ssluzba.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DokumentInputDto {
    private Long id;
    private String naslov;
    private Long ucenik;
    private MultipartFile file;
}
