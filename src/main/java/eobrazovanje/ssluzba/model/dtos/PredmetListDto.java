package eobrazovanje.ssluzba.model.dtos;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PredmetListDto {
    List<PredmetOutputDto> predemti;
}
