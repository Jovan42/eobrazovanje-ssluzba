package eobrazovanje.ssluzba.model.dtos;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OsobaDto {
    private Long id;

    @NotBlank
    @Size(min = 3)
    @ApiModelProperty(value = "Ime", required = true)
    private String ime;

    @NotBlank
    @Size(min = 3)
    @ApiModelProperty(value = "Prezime", required = true)
    private String prezime;

    @NotNull
    @Temporal(TemporalType.DATE)
    @ApiModelProperty(value = "Datum rodjenja", required = true)
    private Date datumRodjenja;

    @NotBlank
    @ApiModelProperty(value = "Mesto rodjenja", required = true)
    private String mestoRodjenja;
}
