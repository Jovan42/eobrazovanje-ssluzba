package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.IspitniRok;
import eobrazovanje.ssluzba.model.enums.TipIspita;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OcenjivanjeDto {
    private TipIspita tipIspita;
    private IspitniRok ispitniRok;
    private Double maksimalniBodovi;
    private Double osvojeniBodovi;
}
