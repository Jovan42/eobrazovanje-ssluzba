package eobrazovanje.ssluzba.model.dtos;

import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PredmetOutputDto {
    private Long id;

    @NotBlank private String naziv;

    @NotNull private Integer espb;

    List<PohadjaOutputDto> pohadja;

    List<PredajeOutputDto> predaje;
}
