package eobrazovanje.ssluzba.model.dtos;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PohadjaOutputDto {
    private Long id;

    @NotNull private UcenikOutputDto ucenik;

    private Integer konacnaOcena;
}
