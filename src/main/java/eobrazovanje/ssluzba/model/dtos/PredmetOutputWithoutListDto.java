package eobrazovanje.ssluzba.model.dtos;

import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PredmetOutputWithoutListDto {
    private Long id;

    @NotBlank private String naziv;

    @NotNull private Integer espb;

    private List<PohadjaOutputDto> pohadja;

    private List<PredajeOutputDto> predaje;
}
