package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.Uloga;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PredajeOutputDto {
    private Long id;

    private PredmetWithoutPredajeDto predmet;

    @NotNull private NastavnikOutputDto nastavnik;

    @NotNull private Uloga uloga;
}
