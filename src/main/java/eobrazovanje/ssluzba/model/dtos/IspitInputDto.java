package eobrazovanje.ssluzba.model.dtos;

import eobrazovanje.ssluzba.model.enums.IspitniRok;
import eobrazovanje.ssluzba.model.enums.TipIspita;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IspitInputDto {
    private Long id;
    @NotNull private TipIspita tipIspita;

    @NotNull private IspitniRok ispitniRok;

    @NotNull private Double maksimumBodova;

    @NotNull private Long predmet;
}
