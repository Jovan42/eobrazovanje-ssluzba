package eobrazovanje.ssluzba.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Pohadja */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Pohadja extends BaseEntity {
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Predmet predmet;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Ucenik ucenik;

    private Integer konacnaOcena;
}
