package eobrazovanje.ssluzba.model;

import eobrazovanje.ssluzba.model.enums.Uloga;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Predaje */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Predaje extends BaseEntity {
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Predmet predmet;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Nastavnik nastavnik;

    @NotNull @Enumerated private Uloga uloga;
}
