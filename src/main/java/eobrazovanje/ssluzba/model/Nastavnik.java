package eobrazovanje.ssluzba.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Nastavnik extends Osoba {
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "nastavnik")
    private List<Predaje> predaje;
}
