package eobrazovanje.ssluzba.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Prijava extends BaseEntity {
    @ManyToOne() private Ucenik ucenik;
    @ManyToOne() private Ispit ispit;

    private Double osvojeniBodovi;
}
