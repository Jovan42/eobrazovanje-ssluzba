package eobrazovanje.ssluzba.model;

import eobrazovanje.ssluzba.model.enums.TipStudija;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Student */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Ucenik extends Osoba {
    @NotBlank private String brojIndeksa;

    @Enumerated private TipStudija tipStudija;

    @NotBlank private String studijskiProgram;

    private String racun;

    private Double stanjeRacun;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ucenik")
    private List<Pohadja> pohadja;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ucenik")
    private List<Uplata> uplate;
}
