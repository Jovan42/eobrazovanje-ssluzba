package eobrazovanje.ssluzba.model.enums;

public enum IspitniRok {
    JANUARKSI,
    FEBRUARSKI,
    APRILSKI,
    JUNSKI,
    JULSKI,
    AVGUSTOVSKI,
    SEPTEMBARSKI,
    OKTOBARSKI
}
