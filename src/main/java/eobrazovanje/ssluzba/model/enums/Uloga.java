package eobrazovanje.ssluzba.model.enums;

public enum Uloga {
    NASTAVNIK,
    ASISTENT,
    DEMONSTRATOR
}
