package eobrazovanje.ssluzba.model.enums;

public enum TipIspita {
    ISPIT,
    KOLOKVIJUM,
    PROJEKAT,
    OSTALO
}
