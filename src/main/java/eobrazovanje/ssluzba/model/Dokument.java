package eobrazovanje.ssluzba.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true) // overriduje equals i hashcode
@Entity //To je tabela u bazi
@NoArgsConstructor
@AllArgsConstructor
public class Dokument extends BaseEntity {
    private String naslov;
    @ManyToOne private Ucenik ucenik;
    private String path;
    private String subject;
    private String author;
    private String keywords;
    private String creator;
    private String creationDate;
    private String modificationDate;

}
