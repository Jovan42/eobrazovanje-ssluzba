package eobrazovanje.ssluzba.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Predmet */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Predmet extends BaseEntity {
    @NotBlank private String naziv;

    @NotNull private Integer espb;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "predmet")
    List<Pohadja> pohadja;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "predmet")
    List<Predaje> predaje;
}
