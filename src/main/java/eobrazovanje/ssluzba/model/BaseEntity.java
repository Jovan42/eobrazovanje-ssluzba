package eobrazovanje.ssluzba.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import lombok.Data;

/** BaseEntity */
@Data //Getter setter lombok dipendensi u mavenu pom.xml
@MappedSuperclass //mapira na bazu
@Inheritance(strategy = InheritanceType.JOINED) // spaja ovaj model i onaj koji ga nasledjuje u jednu tabelu
public class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) //auto increment u bazi
    private Long id;
}
