package eobrazovanje.ssluzba.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/** Uplata */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Uplata extends BaseEntity {
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Ucenik ucenik;

    @NotNull private Double kolicina;

    @Temporal(TemporalType.TIMESTAMP)
    private Date vreme;
}
