package eobrazovanje.ssluzba.model;

import eobrazovanje.ssluzba.model.enums.IspitniRok;
import eobrazovanje.ssluzba.model.enums.TipIspita;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Ispit */

//

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Ispit extends BaseEntity {
    @NotNull @Enumerated private TipIspita tipIspita;

    @NotNull @Enumerated private IspitniRok ispitniRok;

    @NotNull private Double maksimumBodova;

    @NotNull @ManyToOne() private Predmet predmet;

    @OneToMany(mappedBy = "ispit") // Mapira ovaj model pod imenom u navodnicima
    private List<Prijava> prijave;
}
