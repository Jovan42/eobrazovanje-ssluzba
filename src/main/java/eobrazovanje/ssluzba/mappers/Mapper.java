package eobrazovanje.ssluzba.mappers;

import eobrazovanje.ssluzba.model.Dokument;
import eobrazovanje.ssluzba.model.Ispit;
import eobrazovanje.ssluzba.model.Nastavnik;
import eobrazovanje.ssluzba.model.Pohadja;
import eobrazovanje.ssluzba.model.Predaje;
import eobrazovanje.ssluzba.model.Predmet;
import eobrazovanje.ssluzba.model.Prijava;
import eobrazovanje.ssluzba.model.Ucenik;
import eobrazovanje.ssluzba.model.Uplata;
import eobrazovanje.ssluzba.model.dtos.DokumentOutputDto;
import eobrazovanje.ssluzba.model.dtos.IspitInputDto;
import eobrazovanje.ssluzba.model.dtos.IspitOutputDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikInputDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikOutputDto;
import eobrazovanje.ssluzba.model.dtos.PohadjaOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetInputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetWithoutPredajeDto;
import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikInputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import eobrazovanje.ssluzba.model.dtos.UplataInputDto;
import eobrazovanje.ssluzba.model.dtos.UplataOutputDto;
import eobrazovanje.ssluzba.repositories.PredmetRepository;
import eobrazovanje.ssluzba.repositories.UcenikRepository;
import org.mapstruct.Context;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

// Mapira model i DTO jedan na drugi
// Koristi se mapstruct biblioteka
// Biblioteka automatski implementira maper na
@org.mapstruct.Mapper(uses = {UcenikRepository.class, PredmetRepository.class})
public interface Mapper {

    @Named("predmetToLong")
    default long predmetToLong(Predmet predmet) {
        return predmet.getId();
    }

    @Named("nastavnikToLong")
    default long nastavnikToLong(Nastavnik nastavnik) {
        return nastavnik.getId();
    }

    Ispit ispitInputDtoToIspit(IspitInputDto ispitInputDto);

    IspitOutputDto ispitToIspitOutputDto(Ispit ispit);

    default Ucenik longToUcenik(Long ucenik, @Context UcenikRepository ucenikRepository) {
        return ucenikRepository.getOne(ucenik);
    }

    default Predmet longToUcenik(Long predmet, @Context PredmetRepository predmetRepository) {
        return predmetRepository.getOne(predmet);
    }

    UcenikOutputDto ucenikToUcenikOutputDto(Ucenik ucenik);

    @Mapping(target = "pohadja", ignore = true)
    @Mapping(target = "uplate", ignore = true)
    Ucenik ucenikInputDtoToUcenik(UcenikInputDto ucenikInputDto);

    @Named("ucenikToString")
    default String ucenikToString(Ucenik ucenik) {
        return ucenik.getBrojIndeksa();
    }

    Uplata uplataInputDtoToUplata(UplataInputDto uplataDto);

    UplataOutputDto uplataToUplataOutputDto(Uplata uplata);

    @Mapping(target = "predaje", ignore = true)
    Nastavnik nastavnikInputDtoToNastavnik(NastavnikInputDto nastavnikInputDto);

    NastavnikOutputDto nastavnikToNastavnikOutputDto(Nastavnik nastavnik);

    Predmet predmetInputDtoToPredmet(PredmetInputDto predmetInputDto);

    PredmetOutputDto predmetToPredmetOutputDto(Predmet predmet);

    PredajeOutputDto predajeToPredajeOutputDto(Predaje predaje);

    PohadjaOutputDto pohadjaToPohadjaOutputDto(Pohadja predaje);

    PrijavaOutputDto prijavaToPrijavaOutputDto(Prijava prijava);

    default Long ispitToLong(Ispit ispit) {
        return ispit.getId();
    }

    default Long ucenikToLong(Ucenik ucenik) {
        return ucenik.getId();
    }

    PredmetWithoutPredajeDto predajeToPredmetWithoutPredajeDto(Predaje predmet);

    DokumentOutputDto dokumentToDokumentOutputDto(Dokument dokument);
}
