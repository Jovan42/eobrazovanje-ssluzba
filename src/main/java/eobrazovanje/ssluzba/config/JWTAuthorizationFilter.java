package eobrazovanje.ssluzba.config;

import static eobrazovanje.ssluzba.security.SecurityConstants.HEADER_STRING;
import static eobrazovanje.ssluzba.security.SecurityConstants.SECRET;
import static eobrazovanje.ssluzba.security.SecurityConstants.TOKEN_PREFIX;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import eobrazovanje.ssluzba.users.ApplicationUser;
import eobrazovanje.ssluzba.users.ApplicationUserRepository;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    ApplicationUserRepository applicationUserRepository;

    public JWTAuthorizationFilter(
            AuthenticationManager authManager,
            ApplicationUserRepository applicationUserRepository) {
        super(authManager);
        this.applicationUserRepository = applicationUserRepository;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        res.setHeader(
                "Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization");
        res.setHeader("Access-Control-Expose-Headers", "Location");
        String header = req.getHeader(HEADER_STRING);
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            String user =
                    JWT.require(Algorithm.HMAC512(SECRET.getBytes()))
                            .build()
                            .verify(token.replace(TOKEN_PREFIX, ""))
                            .getSubject();
            ApplicationUser applicationUser = applicationUserRepository.findByUsername(user);
            if (user != null) {
                return new UsernamePasswordAuthenticationToken(
                        user, null, applicationUser.getAuthorities());
            }
            return null;
        }
        return null;
    }
}
