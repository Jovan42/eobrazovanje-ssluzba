package eobrazovanje.ssluzba.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;

    private String password;
    private Role role;
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {

        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("GUEST"));
        if (role.equals(Role.ADMIN)) {
            authorities.add(new SimpleGrantedAuthority("NASTAVNIK"));
            authorities.add(new SimpleGrantedAuthority("UCENIK"));
            authorities.add(new SimpleGrantedAuthority("ADMIN"));
        }
        if (role.equals(Role.NASTAVNIK)) authorities.add(new SimpleGrantedAuthority("NASTAVNIK"));
        authorities.add(new SimpleGrantedAuthority("UCENIK"));
        if (role.equals(Role.UCENIK)) authorities.add(new SimpleGrantedAuthority("UCENIK"));
        return authorities;
    }
}
