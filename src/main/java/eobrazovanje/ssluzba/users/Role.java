package eobrazovanje.ssluzba.users;

public enum Role {
    UCENIK,
    NASTAVNIK,
    ADMIN
}
