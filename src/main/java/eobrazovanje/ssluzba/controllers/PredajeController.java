package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.dtos.PredajeListDto;
import eobrazovanje.ssluzba.services.PredajeSerice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PredajeController {
    PredajeSerice predajeSerice;

    @Autowired
    public PredajeController(PredajeSerice predajeSerice) {
        this.predajeSerice = predajeSerice;
    }

    @GetMapping("/{predmetId}/pradavaci")
    public ResponseEntity<PredajeListDto> predavaci(@PathVariable Long predmetId) {
        return new ResponseEntity<>(predajeSerice.getForPredmet(predmetId), HttpStatus.OK);
    }
}
