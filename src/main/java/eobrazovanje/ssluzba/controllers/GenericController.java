package eobrazovanje.ssluzba.controllers;

import java.io.Serializable;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;

public interface GenericController<T, T_COLLECTION, ID extends Serializable> {
    ResponseEntity<T_COLLECTION> get();

    ResponseEntity<T> get(ID id);

    ResponseEntity<T> add(@Valid @RequestBody T t, Errors errors);

    ResponseEntity<T> update(@Valid @RequestBody T t, Errors errors);

    ResponseEntity<Void> delete(ID id);
}
