package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.Dokument;
import eobrazovanje.ssluzba.model.dtos.DokumentInputDto;
import eobrazovanje.ssluzba.model.dtos.DokumentOutputDto;
import eobrazovanje.ssluzba.services.DokumentService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

import java.nio.file.Path;
import java.nio.file.Paths;


@RestController
@RequestMapping(value = "/api/dokumenta")
public class DokumentController {

    private DokumentService dokumentService;

    public DokumentController(DokumentService dokumentService) {
        this.dokumentService = dokumentService;
    }

    @PostMapping("/upload")
    public ResponseEntity<DokumentOutputDto> upload(
            @RequestParam("file") MultipartFile file,
            @RequestParam("naslov") String naslov,
            @RequestParam("ucenik") Long ucenikId) {
        DokumentInputDto dokumentInputDto = new DokumentInputDto();
        dokumentInputDto.setFile(file);
        dokumentInputDto.setId(null);
        dokumentInputDto.setNaslov(naslov);
        dokumentInputDto.setUcenik(ucenikId);
        return new ResponseEntity<>(dokumentService.upload(dokumentInputDto), HttpStatus.OK);
    }

    // ne koriti se
    @PutMapping("/edit")
    public ResponseEntity<DokumentOutputDto> edit(
            @RequestParam("file") MultipartFile file,
            @RequestParam("naslov") String naslov,
            @RequestParam("ucenik") Long ucenikId) {
        DokumentOutputDto dokument = dokumentService
                .getAllForUcenikAndNaslov(ucenikId, naslov);

        if (dokument != null) {
            return new ResponseEntity<>(
                    dokumentService
                            .upload(new DokumentInputDto(dokument.getId(), naslov, ucenikId, file)),
                    HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    dokumentService
                            .upload(new DokumentInputDto(null, naslov, ucenikId, file)),
                    HttpStatus.OK);
        }

    }

    @GetMapping(value = "/download/{id}", produces = "application/pdf")
    public ResponseEntity<ByteArrayResource> download(@PathVariable Long id) {
        return new ResponseEntity<>(dokumentService.download(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteAll/{ucenikId}", produces = "application/pdf")
    public ResponseEntity<Void> deleteAllForUcenik(@PathVariable Long ucenikId) {
        dokumentService.deleteAllByUcenikId(ucenikId);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = "application/pdf")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Dokument dokument = dokumentService.get(id);
        System.out.println(dokument.getPath());
        Path path = Paths.get(dokument.getPath());

        File file = new File(path.toString());
        file.delete();
        dokumentService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
