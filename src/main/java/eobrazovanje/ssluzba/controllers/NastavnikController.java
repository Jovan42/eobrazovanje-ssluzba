package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.dtos.ChangePasswordDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikInputDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;
import eobrazovanje.ssluzba.services.NastanikService;
import eobrazovanje.ssluzba.services.PredajeSerice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/nastavnici")
@Api("Rad sa Nastavnicima")
public class NastavnikController {
    private NastanikService nastanikService;
    private PredajeSerice predajeSerice;

    public NastavnikController(NastanikService nastanikService, PredajeSerice predajeSerice) {
        this.nastanikService = nastanikService;
        this.predajeSerice = predajeSerice;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("")
    @ApiOperation(value = "Dobijanej informacije o svim nastavnicima")
    public ResponseEntity<List<NastavnikOutputDto>> get() {
        return new ResponseEntity<>(nastanikService.getList(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<NastavnikOutputDto> get(@PathVariable Long id) {
        return new ResponseEntity<>(nastanikService.get(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<NastavnikOutputDto> add(
            @Valid @RequestBody NastavnikInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(nastanikService.add(inputDto), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<NastavnikOutputDto> update(
            @Valid @RequestBody NastavnikInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(nastanikService.update(inputDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        nastanikService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{id}/changePass")
    public ResponseEntity<Boolean> changePass(
            @PathVariable Long id, @RequestBody ChangePasswordDto changePasswordDto) {
        return new ResponseEntity<>(
                nastanikService.changePass(id, changePasswordDto), HttpStatus.OK);
    }

    @GetMapping("{id}/predaje")
    public ResponseEntity<List<PredajeOutputDto>> getPohadja(@PathVariable Long id) {
        return new ResponseEntity<>(nastanikService.getPredmete(id), HttpStatus.OK);
    }
}
