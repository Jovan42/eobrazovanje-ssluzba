package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.dtos.PohadjaOutputDto;
import eobrazovanje.ssluzba.services.PohadjaService;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/pohadjanja")
public class PohadjaController {
    private PohadjaService pohadjaService;

    public PohadjaController(PohadjaService pohadjaService) {
        this.pohadjaService = pohadjaService;
    }

    @GetMapping("/notfinished")
    ResponseEntity<List<PohadjaOutputDto>> getNonFinished() {
        return new ResponseEntity<>(pohadjaService.getNotFinished(), HttpStatus.OK);
    }
}
