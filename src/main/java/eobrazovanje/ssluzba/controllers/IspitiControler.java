package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.dtos.IspitInputDto;
import eobrazovanje.ssluzba.model.dtos.IspitOutputDto;
import eobrazovanje.ssluzba.model.dtos.OcenaDto;
import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;
import eobrazovanje.ssluzba.services.IspitService;
import eobrazovanje.ssluzba.services.PrijavaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/ispiti")
public class IspitiControler {
    private IspitService ispitService;
    private PrijavaService prijavaService;

    public IspitiControler(IspitService ispitService, PrijavaService prijavaService) {
        this.ispitService = ispitService;
        this.prijavaService = prijavaService;
    }

    @GetMapping("")
    public ResponseEntity<List<IspitOutputDto>> get() {
        return new ResponseEntity<>(ispitService.getList(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<IspitOutputDto> get(@PathVariable Long id) {
        return new ResponseEntity<>(ispitService.get(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<IspitOutputDto> add(
            @Valid @RequestBody IspitInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(ispitService.add(inputDto), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<IspitOutputDto> update(
            @Valid @RequestBody IspitInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(ispitService.update(inputDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        ispitService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/{ispitId}/prijave/{ucenikId}")
    public ResponseEntity<PrijavaOutputDto> addPrijava(
            @PathVariable Long ispitId, @PathVariable Long ucenikId) {
        return new ResponseEntity<>(prijavaService.addPrijava(ucenikId, ispitId), HttpStatus.OK);
    }

    @DeleteMapping("/{ispitId}/prijave/{ucenikId}")
    public ResponseEntity<PrijavaOutputDto> removePrijava(
            @PathVariable Long ispitId, @PathVariable Long ucenikId) {
        prijavaService.removePrijava(ucenikId, ispitId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("{predmetId}/prijave/{ucenikId}")
    public ResponseEntity<PrijavaOutputDto> oceni(
            @PathVariable Long predmetId,
            @PathVariable Long ucenikId,
            @RequestBody OcenaDto ocena) {
        return new ResponseEntity<>(
                prijavaService.oceni(ucenikId, predmetId, ocena.getOsvojeniBodovi()),
                HttpStatus.OK);
    }
}
