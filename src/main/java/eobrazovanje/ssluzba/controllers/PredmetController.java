package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.dtos.IspitOutputDto;
import eobrazovanje.ssluzba.model.dtos.OcenaPredmetaDto;
import eobrazovanje.ssluzba.model.dtos.OcenjivanjeDto;
import eobrazovanje.ssluzba.model.dtos.PohadjaInputDto;
import eobrazovanje.ssluzba.model.dtos.PohadjaOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeInputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetInputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import eobrazovanje.ssluzba.services.IspitService;
import eobrazovanje.ssluzba.services.PohadjaService;
import eobrazovanje.ssluzba.services.PredajeSerice;
import eobrazovanje.ssluzba.services.PredmetService;
import io.swagger.annotations.Api;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/predmeti")
@Api("Rad sa Predmetima")
public class PredmetController {
    private PredmetService predmetService;
    private PredajeSerice predajeSerice;
    private PohadjaService pohadjaService;
    private IspitService ispitService;

    public PredmetController(
            PredmetService predmetService,
            PredajeSerice predajeSerice,
            PohadjaService pohadjaService,
            IspitService ispitService) {
        this.predmetService = predmetService;
        this.predajeSerice = predajeSerice;
        this.pohadjaService = pohadjaService;
        this.ispitService = ispitService;
    }

    @GetMapping("")
    public ResponseEntity<List<PredmetOutputDto>> get() {
        return new ResponseEntity<>(predmetService.getList(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PredmetOutputDto> get(@PathVariable Long id) {
        return new ResponseEntity<>(predmetService.get(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<PredmetOutputDto> add(
            @Valid @RequestBody PredmetInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(predmetService.add(inputDto), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<PredmetOutputDto> update(
            @Valid @RequestBody PredmetInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(predmetService.update(inputDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        predmetService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("{predmetId}/predaje/{nastavnikId}")
    public ResponseEntity<PredajeOutputDto> add(
            @RequestBody PredajeInputDto inputDto,
            @PathVariable Long predmetId,
            @PathVariable Long nastavnikId) {
        return new ResponseEntity<>(
                predajeSerice.addNastavnik(inputDto, predmetId, nastavnikId), HttpStatus.OK);
    }

    @DeleteMapping("{predmetId}/predaje/{nastavnikId}")
    public ResponseEntity<Void> delete(
            @PathVariable Long predmetId, @PathVariable Long nastavnikId) {
        predajeSerice.removeNastavnik(predmetId, nastavnikId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("{predmetId}/pohadja/{ucenikId}")
    public ResponseEntity<PohadjaOutputDto> addUcenik(
            @RequestBody PohadjaInputDto inputDto,
            @PathVariable Long predmetId,
            @PathVariable Long ucenikId) {
        return new ResponseEntity<>(
                pohadjaService.addUcenik(inputDto, predmetId, ucenikId), HttpStatus.OK);
    }

    @DeleteMapping("{predmetId}/pohadja/{ucenikId}")
    public ResponseEntity<Void> deletUcenike(
            @PathVariable Long predmetId, @PathVariable Long ucenikId) {
        pohadjaService.removeUcenik(predmetId, ucenikId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("{predmetId}/ispiti")
    public ResponseEntity<List<IspitOutputDto>> getIspite(@PathVariable Long predmetId) {
        return new ResponseEntity<>(ispitService.getForPredemt(predmetId), HttpStatus.OK);
    }

    @GetMapping("{predmetId}/ispiti/neprijavljeni/{ucenikId}")
    public ResponseEntity<List<IspitOutputDto>> getIspite(
            @PathVariable Long predmetId, @PathVariable Long ucenikId) {
        return new ResponseEntity<>(
                ispitService.neprijavljeniZaUcenika(predmetId, ucenikId), HttpStatus.OK);
    }

    @GetMapping("{predmetId}/nepolozeni")
    public ResponseEntity<List<UcenikOutputDto>> getNepolozen(@PathVariable Long predmetId) {
        return new ResponseEntity<>(predmetService.getNepolozen(predmetId), HttpStatus.OK);
    }

    @GetMapping("{predmetId}/oceni/{ucenikId}")
    public ResponseEntity<List<OcenjivanjeDto>> getPrijava(
            @PathVariable Long predmetId, @PathVariable Long ucenikId) {
        return new ResponseEntity<>(
                predmetService.getOcenjivanje(predmetId, ucenikId), HttpStatus.OK);
    }

    @PutMapping("{predmetId}/oceni/{ucenikId}")
    public ResponseEntity<Boolean> changeOcena(
            @PathVariable Long predmetId,
            @PathVariable Long ucenikId,
            @RequestBody OcenaPredmetaDto ocena) {
        return new ResponseEntity<>(
                predmetService.oceni(predmetId, ucenikId, ocena), HttpStatus.OK);
    }
}
