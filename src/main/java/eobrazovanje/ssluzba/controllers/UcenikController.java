package eobrazovanje.ssluzba.controllers;

import eobrazovanje.ssluzba.model.dtos.ChangePasswordDto;
import eobrazovanje.ssluzba.model.dtos.DokumentOutputDto;
import eobrazovanje.ssluzba.model.dtos.IzmenaRacunaDto;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikInputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import eobrazovanje.ssluzba.model.dtos.UplataOutputDto;
import eobrazovanje.ssluzba.services.DokumentService;
import eobrazovanje.ssluzba.services.UcenikService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/ucenici")
public class UcenikController {
    UcenikService ucenikService;
    DokumentService dokumentService;

    public UcenikController(UcenikService ucenikService, DokumentService dokumentService) {
        this.ucenikService = ucenikService;
        this.dokumentService = dokumentService;
    }

    @GetMapping("")
    public ResponseEntity<List<UcenikOutputDto>> get() {
        return new ResponseEntity<>(ucenikService.getList(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UcenikOutputDto> get(@PathVariable Long id) {
        return new ResponseEntity<>(ucenikService.get(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/uplate")
    public ResponseEntity<List<UplataOutputDto>> getUplate(@PathVariable Long id) {
        return new ResponseEntity<>(ucenikService.get(id).getUplate(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<UcenikOutputDto> add(
            @Valid @RequestBody UcenikInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(ucenikService.add(inputDto), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<UcenikOutputDto> update(
            @Valid @RequestBody UcenikInputDto inputDto, Errors errors) {
        return new ResponseEntity<>(ucenikService.update(inputDto), HttpStatus.OK);
    }

    @PutMapping("/{id}/racun")
    public ResponseEntity<Double> updateStanjeRacuna(
            @PathVariable Long id, @RequestBody IzmenaRacunaDto inputDto) {
        return new ResponseEntity<>(
                ucenikService.izmenaRacuna(id, inputDto.getIzmenaRacuna()), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        dokumentService.deleteAllByUcenikId(id);
        ucenikService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("{id}/pohadja")
    public ResponseEntity<List<PredmetOutputDto>> getPohadja(@PathVariable Long id) {
        return new ResponseEntity<>(ucenikService.getForUcenik(id), HttpStatus.OK);
    }

    @GetMapping("{id}/pohadja/polozeni")
    public ResponseEntity<List<PredmetOutputDto>> getPohadjaPolozeni(@PathVariable Long id) {
        return new ResponseEntity<>(ucenikService.getForUcenikPolozeni(id), HttpStatus.OK);
    }

    @GetMapping("{id}/pohadja/nepolozeni")
    public ResponseEntity<List<PredmetOutputDto>> getPohadjanepolozeni(@PathVariable Long id) {
        return new ResponseEntity<>(ucenikService.getForUcenikNepolozeni(id), HttpStatus.OK);
    }

    @PutMapping("{id}/changePass")
    public ResponseEntity<Boolean> changePass(
            @PathVariable Long id, @RequestBody ChangePasswordDto changePasswordDto) {
        return new ResponseEntity<>(ucenikService.changePass(id, changePasswordDto), HttpStatus.OK);
    }

    @GetMapping("{id}/prijave/null")
    public ResponseEntity<List<PrijavaOutputDto>> priajveNull(@PathVariable Long id) {
        return new ResponseEntity<>(ucenikService.getPrijaveBodoviNull(id), HttpStatus.OK);
    }

    @GetMapping("{id}/dokumenta")
    public ResponseEntity<List<DokumentOutputDto>> getDokumenta(@PathVariable Long id) {
        return new ResponseEntity<>(dokumentService.getAllForUcenik(id), HttpStatus.OK);
    }
}
