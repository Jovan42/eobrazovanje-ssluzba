package eobrazovanje.ssluzba.aspects;

import eobrazovanje.ssluzba.exception.BadRequestException;
import eobrazovanje.ssluzba.model.dtos.ErrorMessage;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

@Aspect
@Component
@Slf4j
public class ValidateAspect {

    @Before("execution(public * eobrazovanje.ssluzba.controllers.*.*(..)) && args(.., errors)")
    public void beforeController(Errors errors) {
        log.debug("@Before aspect");
        if (errors != null && errors.hasErrors()) {
            List<String> errorMessages = new ArrayList<>();
            for (FieldError fe : errors.getFieldErrors()) {
                errorMessages.add(fe.getField() + " " + fe.getDefaultMessage());
            }
            throw new BadRequestException(new ErrorMessage(400, "Bad Request", errorMessages));
        }
    }
}
