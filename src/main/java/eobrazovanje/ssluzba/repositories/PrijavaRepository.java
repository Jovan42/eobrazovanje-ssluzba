package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Prijava;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrijavaRepository extends JpaRepository<Prijava, Long> {
    Prijava getAllByUcenik_IdAndIspit_Id(Long ucenikId, Long ispitId);

    List<Prijava> getAllByUcenik_IdAndAndOsvojeniBodoviNot(Long ucenikId, Double bodovi);
}
