package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Ucenik;
import org.springframework.data.jpa.repository.JpaRepository;

/** UcenikRepository */
public interface UcenikRepository extends JpaRepository<Ucenik, Long> {}
