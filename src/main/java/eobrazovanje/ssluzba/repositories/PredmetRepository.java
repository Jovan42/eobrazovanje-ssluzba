package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Predmet;
import org.springframework.data.jpa.repository.JpaRepository;

/** PredmetRepository */
// Autmatski odradi CRUD za bazu
public interface PredmetRepository extends JpaRepository<Predmet, Long> {}
