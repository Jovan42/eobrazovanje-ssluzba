package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Uplata;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UplataRepository extends JpaRepository<Uplata, Long> {}
