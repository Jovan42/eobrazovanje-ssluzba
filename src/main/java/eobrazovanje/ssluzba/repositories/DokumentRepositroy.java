package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Dokument;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DokumentRepositroy extends JpaRepository<Dokument, Long> {
    List<Dokument> getAllByUcenik_Id(Long ucenikId);
    Dokument getByUcenik_IdAndNaslov(Long ucenikId, String naslov);
    void deleteAllByUcenik_Id(Long ucenikId);
}
