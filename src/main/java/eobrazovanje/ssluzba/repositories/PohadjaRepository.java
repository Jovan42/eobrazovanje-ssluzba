package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Pohadja;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/** PohadjaRepository */
public interface PohadjaRepository extends CrudRepository<Pohadja, Long> {
    List<Pohadja> findByUcenik_Id(Long id);

    List<Pohadja> findByPredmet(String predemt);

    Pohadja getAllByUcenik_IdAndPredmet_Id(Long ucenikId, Long predemtId);

    List<Pohadja> getAllByKonacnaOcenaOrKonacnaOcena(Long ocena, Long ocena2);

    List<Pohadja> getAllByKonacnaOcenaOrKonacnaOcenaAndPredmet_Id(
            Integer ocena, Integer socena2, Long predmetId);
}
