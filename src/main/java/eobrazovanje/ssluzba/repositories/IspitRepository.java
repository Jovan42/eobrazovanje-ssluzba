package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Ispit;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IspitRepository extends JpaRepository<Ispit, Long> {
    List<Ispit> findAllByPredmet_Id(Long predmetId);
}
