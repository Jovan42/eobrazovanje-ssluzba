package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Nastavnik;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NastavnikRepository extends JpaRepository<Nastavnik, Long> {}
