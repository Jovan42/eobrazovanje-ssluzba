package eobrazovanje.ssluzba.repositories;

import eobrazovanje.ssluzba.model.Predaje;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PredajeRepository extends JpaRepository<Predaje, Long> {
    List<Predaje> getAllByNastavnik_Id(Long nastavnikId);

    List<Predaje> getAllByPredmet_Id(Long prednetId);

    Predaje getAllByNastavnik_IdAndPredmet_Id(Long nastavnikId, Long prednetId);
}
