package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Ispit;
import eobrazovanje.ssluzba.model.Prijava;
import eobrazovanje.ssluzba.model.dtos.IspitInputDto;
import eobrazovanje.ssluzba.model.dtos.IspitListDto;
import eobrazovanje.ssluzba.model.dtos.IspitOutputDto;
import eobrazovanje.ssluzba.repositories.IspitRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class IspitServiceImpl implements IspitService {

    private IspitRepository ispitRepository;
    private Mapper mapper;

    public IspitServiceImpl(IspitRepository ispitRepository, Mapper mapper) {
        this.ispitRepository = ispitRepository;
        this.mapper = mapper;
    }

    @Override
    public IspitListDto get() {
        return null;
    }

    @Override
    public IspitOutputDto get(Long aLong) {
        return mapper.ispitToIspitOutputDto(ispitRepository.findById(aLong).get());
    }

    @Override
    public IspitOutputDto add(IspitInputDto ispitInputDto) {
        ispitInputDto.setId(null);
        Ispit ispit = mapper.ispitInputDtoToIspit(ispitInputDto);
        return mapper.ispitToIspitOutputDto(ispitRepository.save(ispit));
    }

    @Override
    public IspitOutputDto update(IspitInputDto ispitInputDto) {
        Ispit ispit = mapper.ispitInputDtoToIspit(ispitInputDto);
        return mapper.ispitToIspitOutputDto(ispitRepository.save(ispit));
    }

    @Override
    public void delete(Long aLong) {
        ispitRepository.deleteById(aLong);
    }

    @Override
    public List<IspitOutputDto> getList() {
        return ispitRepository.findAll().stream()
                .map(mapper::ispitToIspitOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IspitOutputDto> getForPredemt(Long predmetId) {
        return ispitRepository.findAllByPredmet_Id(predmetId).stream()
                .map(mapper::ispitToIspitOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IspitOutputDto> neprijavljeniZaUcenika(Long predmetId, Long ucenikId) {
        List<Ispit> ispiti = ispitRepository.findAllByPredmet_Id(predmetId);
        List<Ispit> ispitiForReturn = new ArrayList<>();
        ispiti.forEach(
                ispit -> {
                    int i = 0;
                    for (Prijava prijava : ispit.getPrijave()) {
                        if (prijava.getUcenik().getId().equals(ucenikId)) i = 1;
                    }
                    if (i == 0) ispitiForReturn.add(ispit);
                });

        return ispitiForReturn.stream()
                .map(mapper::ispitToIspitOutputDto)
                .collect(Collectors.toList());
    }
}
