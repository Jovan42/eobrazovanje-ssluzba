package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.ChangePasswordDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikInputDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikListDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;
import io.swagger.annotations.Api;
import java.util.List;

@Api("Rad sa nastavnicima")
public interface NastanikService
        extends GenericService<NastavnikOutputDto, NastavnikInputDto, NastavnikListDto, Long> {
    List<NastavnikOutputDto> getList();

    List<PredajeOutputDto> getPredmete(Long nastanikId);

    Boolean changePass(Long id, ChangePasswordDto changePasswordDto);
}
