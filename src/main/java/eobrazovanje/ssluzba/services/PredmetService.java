package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.OcenaPredmetaDto;
import eobrazovanje.ssluzba.model.dtos.OcenjivanjeDto;
import eobrazovanje.ssluzba.model.dtos.PredmetInputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetListDto;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import java.util.List;

public interface PredmetService
        extends GenericService<PredmetOutputDto, PredmetInputDto, PredmetListDto, Long> {
    List<PredmetOutputDto> getList();

    List<UcenikOutputDto> getNepolozen(Long predmetId);

    List<OcenjivanjeDto> getOcenjivanje(Long predmetId, Long ucenikId);

    Boolean oceni(Long predmetId, Long ucenikId, OcenaPredmetaDto ocena);
}
