package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Dokument;
import eobrazovanje.ssluzba.model.dtos.DokumentInputDto;
import eobrazovanje.ssluzba.model.dtos.DokumentOutputDto;
import eobrazovanje.ssluzba.repositories.DokumentRepositroy;
import eobrazovanje.ssluzba.repositories.UcenikRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFileAttributes;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class DokumentServiceImpl implements DokumentService {
    private DokumentRepositroy dokumentRepositroy;
    private UcenikRepository ucenikRepository;
    private Mapper mapper;

    public DokumentServiceImpl(
            DokumentRepositroy dokumentRepositroy,
            UcenikRepository ucenikRepository,
            Mapper mapper) {
        this.dokumentRepositroy = dokumentRepositroy;
        this.ucenikRepository = ucenikRepository;
        this.mapper = mapper;
    }

    @Override
    public List<Dokument> get() {
        return null;
    }

    @Override
    public Dokument get(Long aLong) {
        return dokumentRepositroy.findById(aLong).get();
    }

    @Override
    public Dokument add(DokumentInputDto dokumentInputDto) {
        return null;
    }

    @Override
    public Dokument update(DokumentInputDto dokumentInputDto) {
        return null;
    }

    @Override
    public void delete(Long aLong) {
        dokumentRepositroy.deleteById(aLong);
    }

    @Override
    public DokumentOutputDto upload(DokumentInputDto dokumentInputDto) {
        try {
            File dir = new File(".\\dokumenta\\" + dokumentInputDto.getUcenik());
            if (!dir.exists()) dir.mkdir();

            MultipartFile file = dokumentInputDto.getFile(); // Fajl koji si poslao
            byte[] bytes = file.getBytes();
            String strPath =
                    ".\\dokumenta\\"
                            + dokumentInputDto.getUcenik()
                            + "\\"
                            + dokumentInputDto.getNaslov();
            Path path = Paths.get(strPath);
            Files.write(path, bytes);

            PDDocument doc = PDDocument.load(new File(strPath));
            PDDocumentInformation info = doc.getDocumentInformation();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
            Dokument dokument =
                    new Dokument(
                            dokumentInputDto.getNaslov(),
                            ucenikRepository.getOne(dokumentInputDto.getUcenik()),
                            path.toAbsolutePath().toString(),
                            info.getSubject(),
                            info.getAuthor(),
                            info.getKeywords(),
                            info.getCreator(),
                            simpleDateFormat.format(info.getCreationDate().getTime()),
                            simpleDateFormat.format(info.getModificationDate().getTime()));
            dokumentRepositroy.save(dokument);
            return mapper.dokumentToDokumentOutputDto(dokument);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ByteArrayResource download(Long id) {
        try {
            Dokument dokument = dokumentRepositroy.getOne(id);
            Path path = Paths.get(dokument.getPath());
            return new ByteArrayResource(Files.readAllBytes(path));

        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public List<DokumentOutputDto> getAllForUcenik(Long ucenikId) {
        return dokumentRepositroy.getAllByUcenik_Id(ucenikId).stream() // Uzimas jedan po jedan dokument i nesto radis sa njim
                .map(mapper::dokumentToDokumentOutputDto)
                .collect(Collectors.toList()); // Dodaje u listu
    }

    @Override
    public DokumentOutputDto getAllForUcenikAndNaslov(Long ucenikId, String naslov) {
        return mapper.dokumentToDokumentOutputDto(dokumentRepositroy.getByUcenik_IdAndNaslov(ucenikId, naslov));
    }

    @Override
    @Transactional
    public void deleteAllByUcenikId(Long ucenikId) {
        dokumentRepositroy.deleteAllByUcenik_Id(ucenikId);
        try {
            FileUtils.deleteDirectory(new File(".\\dokumenta\\" + ucenikId));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
