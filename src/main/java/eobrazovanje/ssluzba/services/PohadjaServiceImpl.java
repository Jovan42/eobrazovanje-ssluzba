package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Pohadja;
import eobrazovanje.ssluzba.model.dtos.PohadjaInputDto;
import eobrazovanje.ssluzba.model.dtos.PohadjaOutputDto;
import eobrazovanje.ssluzba.repositories.NastavnikRepository;
import eobrazovanje.ssluzba.repositories.PohadjaRepository;
import eobrazovanje.ssluzba.repositories.PredmetRepository;
import eobrazovanje.ssluzba.repositories.UcenikRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class PohadjaServiceImpl implements PohadjaService {
    private PohadjaRepository pohadjaRepository;
    private NastavnikRepository nastavnikRepository;
    private PredmetRepository predmetRepository;
    private UcenikRepository ucenikRepository;
    private Mapper mapper;

    public PohadjaServiceImpl(
            PohadjaRepository pohadjaRepository,
            NastavnikRepository nastavnikRepository,
            PredmetRepository predmetRepository,
            UcenikRepository ucenikRepository,
            Mapper mapper) {
        this.pohadjaRepository = pohadjaRepository;
        this.nastavnikRepository = nastavnikRepository;
        this.predmetRepository = predmetRepository;
        this.ucenikRepository = ucenikRepository;
        this.mapper = mapper;
    }

    @Override
    public PohadjaOutputDto addUcenik(
            PohadjaInputDto pohadjaInputDto, Long predmetId, Long ucenikId) {
        Pohadja pohadja = new Pohadja();
        pohadja.setUcenik(ucenikRepository.getOne(ucenikId));
        pohadja.setPredmet(predmetRepository.getOne(predmetId));
        pohadja.setKonacnaOcena(pohadjaInputDto.getKonacnaOcena());
        return mapper.pohadjaToPohadjaOutputDto(pohadjaRepository.save(pohadja));
    }

    @Override
    public void removeUcenik(Long predmetId, Long ucenikId) {
        pohadjaRepository.delete(
                pohadjaRepository.getAllByUcenik_IdAndPredmet_Id(ucenikId, predmetId));
    }

    @Override
    public List<PohadjaOutputDto> getNotFinished() {
        return pohadjaRepository.getAllByKonacnaOcenaOrKonacnaOcena(5L, null).stream()
                .map(mapper::pohadjaToPohadjaOutputDto)
                .collect(Collectors.toList());
    }
}
