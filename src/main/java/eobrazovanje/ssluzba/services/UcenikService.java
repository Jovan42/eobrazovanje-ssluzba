package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.ChangePasswordDto;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikInputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikListDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import java.util.List;

/** UcenikService */
public interface UcenikService
        extends GenericService<UcenikOutputDto, UcenikInputDto, UcenikListDto, Long> {
    Double izmenaRacuna(Long id, Double kolicina);

    List<UcenikOutputDto> getList();

    List<PredmetOutputDto> getForUcenik(Long ucenikId);

    Boolean changePass(Long id, ChangePasswordDto changePasswordDto);

    List<PredmetOutputDto> getForUcenikPolozeni(Long ucenikId);

    List<PredmetOutputDto> getForUcenikNepolozeni(Long ucenikId);

    List<PrijavaOutputDto> getPrijaveBodoviNull(Long ucenikId);
}
