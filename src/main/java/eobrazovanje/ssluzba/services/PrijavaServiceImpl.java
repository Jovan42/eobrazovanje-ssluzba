package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Prijava;
import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import eobrazovanje.ssluzba.model.dtos.UplataInputDto;
import eobrazovanje.ssluzba.repositories.IspitRepository;
import eobrazovanje.ssluzba.repositories.PrijavaRepository;
import eobrazovanje.ssluzba.repositories.UcenikRepository;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class PrijavaServiceImpl implements PrijavaService {
    private PrijavaRepository prijavaRepository;
    private UcenikRepository ucenikRepository;
    private IspitRepository ispitRepository;
    private UplataService uplataService;
    private Mapper mapper;

    public PrijavaServiceImpl(
            PrijavaRepository prijavaRepository,
            UcenikRepository ucenikRepository,
            IspitRepository ispitRepository,
            UplataService uplataService,
            Mapper mapper) {
        this.prijavaRepository = prijavaRepository;
        this.ucenikRepository = ucenikRepository;
        this.ispitRepository = ispitRepository;
        this.uplataService = uplataService;
        this.mapper = mapper;
    }

    @Override
    public PrijavaOutputDto addPrijava(Long ucenikId, Long ispitId) {
        Prijava prijava =
                new Prijava(ucenikRepository.getOne(ucenikId), ispitRepository.getOne(ispitId), 0D);
        prijava.setOsvojeniBodovi(null);
        UcenikOutputDto ucenikOutputDto =
                mapper.ucenikToUcenikOutputDto(ucenikRepository.getOne(ucenikId));
        UplataInputDto uplata = new UplataInputDto(null, -200D, ucenikOutputDto, new Date());
        uplataService.add(uplata);
        return mapper.prijavaToPrijavaOutputDto(prijavaRepository.save(prijava));
    }

    @Override
    public PrijavaOutputDto oceni(Long ucenikId, Long ispitId, Double bodovi) {
        Prijava prijava = prijavaRepository.getAllByUcenik_IdAndIspit_Id(ucenikId, ispitId);
        prijava.setOsvojeniBodovi(bodovi);
        return mapper.prijavaToPrijavaOutputDto(prijavaRepository.save(prijava));
    }

    @Override
    public void removePrijava(Long ucenikId, Long ispitId) {
        prijavaRepository.delete(prijavaRepository.getAllByUcenik_IdAndIspit_Id(ucenikId, ispitId));
    }
}
