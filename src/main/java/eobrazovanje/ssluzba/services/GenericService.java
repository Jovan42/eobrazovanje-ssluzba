package eobrazovanje.ssluzba.services;

import java.io.Serializable;

/** GenericService */
public interface GenericService<T_OUT, T_IN, T_COLLECTION, ID extends Serializable> {

    T_COLLECTION get();

    T_OUT get(ID id);

    T_OUT add(T_IN t);

    T_OUT update(T_IN t);

    void delete(ID id);
}
