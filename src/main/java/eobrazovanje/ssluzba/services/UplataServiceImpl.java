package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Uplata;
import eobrazovanje.ssluzba.model.dtos.UplataInputDto;
import eobrazovanje.ssluzba.model.dtos.UplataListDto;
import eobrazovanje.ssluzba.model.dtos.UplataOutputDto;
import eobrazovanje.ssluzba.repositories.UplataRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UplataServiceImpl implements UplataService {
    UplataRepository uplataRepository;
    UcenikService ucenikService;
    Mapper mapper;

    public UplataServiceImpl(
            UplataRepository uplataRepository, UcenikService ucenikService, Mapper mapper) {
        this.uplataRepository = uplataRepository;
        this.ucenikService = ucenikService;
        this.mapper = mapper;
    }

    @Override
    public UplataListDto get() {
        List<UplataOutputDto> ucenici =
                uplataRepository.findAll().stream()
                        .map(mapper::uplataToUplataOutputDto)
                        .collect(Collectors.toList());
        return new UplataListDto(ucenici);
    }

    @Override
    public List<UplataOutputDto> getList() {
        return uplataRepository.findAll().stream()
                .map(mapper::uplataToUplataOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public UplataOutputDto get(Long aLong) {
        return mapper.uplataToUplataOutputDto(uplataRepository.findById(aLong).get());
    }

    @Override
    @Transactional
    public UplataOutputDto add(UplataInputDto uplataDto) {
        uplataDto.setId(null);
        Uplata uplata = mapper.uplataInputDtoToUplata(uplataDto);
        return mapper.uplataToUplataOutputDto(uplataRepository.save(uplata));
    }

    @Override
    @Transactional
    public UplataOutputDto update(UplataInputDto uplataDto) {
        Uplata u = mapper.uplataInputDtoToUplata(uplataDto);
        return mapper.uplataToUplataOutputDto(uplataRepository.save(u));
    }

    @Override
    @Transactional
    public void delete(Long aLong) {
        Uplata uplata = uplataRepository.findById(aLong).get();
        uplataRepository.deleteById(aLong);
    }
}
