package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.IspitInputDto;
import eobrazovanje.ssluzba.model.dtos.IspitListDto;
import eobrazovanje.ssluzba.model.dtos.IspitOutputDto;
import java.util.List;

public interface IspitService
        extends GenericService<IspitOutputDto, IspitInputDto, IspitListDto, Long> {
    List<IspitOutputDto> getList();

    List<IspitOutputDto> getForPredemt(Long predmetId);

    List<IspitOutputDto> neprijavljeniZaUcenika(Long predmetId, Long ucenikId);
}
