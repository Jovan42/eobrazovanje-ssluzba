package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;

public interface PrijavaService {
    PrijavaOutputDto addPrijava(Long ucenikId, Long ispitId);

    PrijavaOutputDto oceni(Long ucenikId, Long ispitId, Double bodovi);

    void removePrijava(Long ucenikId, Long ispitId);
}
