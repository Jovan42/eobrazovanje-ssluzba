package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Predaje;
import eobrazovanje.ssluzba.model.dtos.PredajeInputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeListDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;
import eobrazovanje.ssluzba.repositories.NastavnikRepository;
import eobrazovanje.ssluzba.repositories.PredajeRepository;
import eobrazovanje.ssluzba.repositories.PredmetRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class PredajeServiceImpl implements PredajeSerice {
    private PredajeRepository predajeRepository;
    private NastavnikRepository nastavnikRepository;
    private PredmetRepository predmetRepository;
    private Mapper mapper;

    public PredajeServiceImpl(
            PredajeRepository predajeRepository,
            NastavnikRepository nastavnikRepository,
            PredmetRepository predmetRepository,
            Mapper mapper) {
        this.predajeRepository = predajeRepository;
        this.nastavnikRepository = nastavnikRepository;
        this.predmetRepository = predmetRepository;
        this.mapper = mapper;
    }

    @Override
    public PredajeListDto get() {
        return null;
    }

    @Override
    public PredajeOutputDto get(Long aLong) {
        return null;
    }

    @Override
    public PredajeOutputDto add(PredajeInputDto predajeInputDto) {
        return null;
    }

    @Override
    public PredajeOutputDto update(PredajeInputDto predajeInputDto) {
        return null;
    }

    @Override
    public void delete(Long aLong) {}

    @Override
    public PredajeListDto getForPredmet(Long predmetId) {
        List<PredajeOutputDto> predavanja =
                predajeRepository.getAllByPredmet_Id(predmetId).stream()
                        .map(mapper::predajeToPredajeOutputDto)
                        .collect(Collectors.toList());
        return new PredajeListDto(predavanja);
    }

    @Override
    public PredajeOutputDto addNastavnik(
            PredajeInputDto predajeInputDto, Long predmetId, Long nastanikId) {
        Predaje predaje = new Predaje();
        predaje.setNastavnik(nastavnikRepository.getOne(nastanikId));
        predaje.setPredmet(predmetRepository.getOne(predmetId));
        predaje.setUloga(predajeInputDto.getUloga());
        return mapper.predajeToPredajeOutputDto(predajeRepository.save(predaje));
    }

    @Override
    public void removeNastavnik(Long predmetId, Long nastanikId) {
        predajeRepository.delete(
                predajeRepository.getAllByNastavnik_IdAndPredmet_Id(nastanikId, predmetId));
    }
}
