package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.PredajeInputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeListDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;

public interface PredajeSerice
        extends GenericService<PredajeOutputDto, PredajeInputDto, PredajeListDto, Long> {
    PredajeListDto getForPredmet(Long predmetId);

    PredajeOutputDto addNastavnik(PredajeInputDto predajeInputDto, Long predmetId, Long nastanikId);

    void removeNastavnik(Long predmetId, Long nastanikId);
}
