package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Pohadja;
import eobrazovanje.ssluzba.model.Predmet;
import eobrazovanje.ssluzba.model.Ucenik;
import eobrazovanje.ssluzba.model.dtos.OcenaPredmetaDto;
import eobrazovanje.ssluzba.model.dtos.OcenjivanjeDto;
import eobrazovanje.ssluzba.model.dtos.PredmetInputDto;
import eobrazovanje.ssluzba.model.dtos.PredmetListDto;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import eobrazovanje.ssluzba.repositories.IspitRepository;
import eobrazovanje.ssluzba.repositories.PohadjaRepository;
import eobrazovanje.ssluzba.repositories.PredmetRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class PredmetServiceImpl implements PredmetService {
    private PredmetRepository predmetRepository;
    private PohadjaRepository pohadjaRepository;
    private IspitRepository ispitRepository;
    private Mapper mapper;

    public PredmetServiceImpl(
            PredmetRepository predmetRepository,
            PohadjaRepository pohadjaRepository,
            IspitRepository ispitRepository,
            Mapper mapper) {
        this.predmetRepository = predmetRepository;
        this.pohadjaRepository = pohadjaRepository;
        this.ispitRepository = ispitRepository;
        this.mapper = mapper;
    }

    @Override
    public PredmetListDto get() {
        List<PredmetOutputDto> predemti =
                predmetRepository.findAll().stream()
                        .map(mapper::predmetToPredmetOutputDto)
                        .collect(Collectors.toList());
        return new PredmetListDto(predemti);
    }

    @Override
    public List<PredmetOutputDto> getList() {
        return predmetRepository.findAll().stream()
                .map(mapper::predmetToPredmetOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<UcenikOutputDto> getNepolozen(Long predmetId) {
        List<Ucenik> ucenici = new ArrayList<>();
        pohadjaRepository
                .getAllByKonacnaOcenaOrKonacnaOcenaAndPredmet_Id(5, null, predmetId)
                .forEach(pohadja -> ucenici.add(pohadja.getUcenik()));

        return ucenici.stream().map(mapper::ucenikToUcenikOutputDto).collect(Collectors.toList());
    }

    @Override
    public List<OcenjivanjeDto> getOcenjivanje(Long predmetId, Long ucenikId) {
        List<OcenjivanjeDto> ocenjivanjeDtos = new ArrayList<>();
        ispitRepository
                .findAllByPredmet_Id(predmetId)
                .forEach(
                        ispit -> {
                            ispit.getPrijave()
                                    .forEach(
                                            prijava -> {
                                                if (prijava.getUcenik().getId().equals(ucenikId)
                                                        && prijava.getOsvojeniBodovi() != null) {
                                                    ocenjivanjeDtos.add(
                                                            new OcenjivanjeDto(
                                                                    ispit.getTipIspita(),
                                                                    ispit.getIspitniRok(),
                                                                    ispit.getMaksimumBodova(),
                                                                    prijava.getOsvojeniBodovi()));
                                                }
                                            });
                        });

        return ocenjivanjeDtos;
    }

    @Override
    public Boolean oceni(Long predmetId, Long ucenikId, OcenaPredmetaDto ocena) {
        Pohadja pohadja = pohadjaRepository.getAllByUcenik_IdAndPredmet_Id(ucenikId, predmetId);
        pohadja.setKonacnaOcena(ocena.getOcena());
        pohadjaRepository.save(pohadja);
        return true;
    }

    @Override
    public PredmetOutputDto get(Long aLong) {
        return mapper.predmetToPredmetOutputDto(predmetRepository.findById(aLong).get());
    }

    @Override
    public PredmetOutputDto add(PredmetInputDto predmetInputDto) {
        predmetInputDto.setId(null);
        Predmet predmet = mapper.predmetInputDtoToPredmet(predmetInputDto);
        return mapper.predmetToPredmetOutputDto(predmetRepository.save(predmet));
    }

    @Override
    public PredmetOutputDto update(PredmetInputDto predmetInputDto) {
        Predmet predmet = mapper.predmetInputDtoToPredmet(predmetInputDto);
        return mapper.predmetToPredmetOutputDto(predmetRepository.save(predmet));
    }

    @Override
    public void delete(Long aLong) {
        predmetRepository.deleteById(aLong);
    }
}
