package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.PohadjaInputDto;
import eobrazovanje.ssluzba.model.dtos.PohadjaOutputDto;
import java.util.List;

public interface PohadjaService {
    PohadjaOutputDto addUcenik(PohadjaInputDto pohadjaInputDto, Long predmetId, Long ucenikId);

    void removeUcenik(Long predmetId, Long ucenikId);

    List<PohadjaOutputDto> getNotFinished();
}
