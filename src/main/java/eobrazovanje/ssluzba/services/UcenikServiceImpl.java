package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.config.SecurityConfig;
import eobrazovanje.ssluzba.exception.BadRequestException;
import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Ucenik;
import eobrazovanje.ssluzba.model.Uplata;
import eobrazovanje.ssluzba.model.dtos.ChangePasswordDto;
import eobrazovanje.ssluzba.model.dtos.ErrorMessage;
import eobrazovanje.ssluzba.model.dtos.PredmetOutputDto;
import eobrazovanje.ssluzba.model.dtos.PrijavaOutputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikInputDto;
import eobrazovanje.ssluzba.model.dtos.UcenikListDto;
import eobrazovanje.ssluzba.model.dtos.UcenikOutputDto;
import eobrazovanje.ssluzba.repositories.PohadjaRepository;
import eobrazovanje.ssluzba.repositories.PrijavaRepository;
import eobrazovanje.ssluzba.repositories.UcenikRepository;
import eobrazovanje.ssluzba.repositories.UplataRepository;
import eobrazovanje.ssluzba.users.ApplicationUser;
import eobrazovanje.ssluzba.users.ApplicationUserRepository;
import eobrazovanje.ssluzba.users.Role;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/** UcenikServiceImpl */
@Service
public class UcenikServiceImpl implements UcenikService {
    private Mapper mapper;
    private UcenikRepository ucenikRepository;
    private PohadjaRepository pohadjaRepository;
    private UplataRepository uplataRepository;
    private ApplicationUserRepository applicationUserRepository;
    private PrijavaRepository prijavaRepository;
    SecurityConfig securityConfig;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UcenikServiceImpl(
            Mapper mapper,
            UcenikRepository ucenikRepository,
            PohadjaRepository pohadjaRepository,
            UplataRepository uplataRepository,
            ApplicationUserRepository applicationUserRepository,
            PrijavaRepository prijavaRepository,
            SecurityConfig securityConfig,
            BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.mapper = mapper;
        this.ucenikRepository = ucenikRepository;
        this.pohadjaRepository = pohadjaRepository;
        this.uplataRepository = uplataRepository;
        this.applicationUserRepository = applicationUserRepository;
        this.securityConfig = securityConfig;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.prijavaRepository = prijavaRepository;
    }

    @Override
    @Transactional
    public UcenikListDto get() {
        List<UcenikOutputDto> ucenici =
                ucenikRepository.findAll().stream()
                        .map(mapper::ucenikToUcenikOutputDto)
                        .collect(Collectors.toList());
        return new UcenikListDto(ucenici);
    }

    @Override
    @Transactional
    public UcenikOutputDto get(Long id) {
        UcenikOutputDto ucenik =
                mapper.ucenikToUcenikOutputDto(ucenikRepository.findById(id).get());
        ApplicationUser appUSer =
                applicationUserRepository.findAllByUserIdAndRole(ucenik.getId(), Role.UCENIK);
        ucenik.setUsername(appUSer.getUsername());
        ucenik.setPassword(appUSer.getPassword());
        return ucenik;
    }

    @Override
    @Transactional
    public Double izmenaRacuna(Long id, Double kolicina) {
        Ucenik ucenik = ucenikRepository.getOne(id);
        ucenik.setStanjeRacun(ucenik.getStanjeRacun() + kolicina);
        ucenikRepository.save(ucenik);
        uplataRepository.save(new Uplata(ucenik, kolicina, new Date()));
        return ucenik.getStanjeRacun();
    }

    @Override
    @Transactional
    public List<UcenikOutputDto> getList() {
        List<UcenikOutputDto> ucenici =
                ucenikRepository.findAll().stream()
                        .map(mapper::ucenikToUcenikOutputDto)
                        .collect(Collectors.toList());

        ucenici.forEach(
                ucenik -> {
                    ApplicationUser appUSer =
                            applicationUserRepository.findAllByUserIdAndRole(
                                    ucenik.getId(), Role.UCENIK);
                    System.out.println("\n\n\t" + appUSer);
                    ucenik.setUsername(appUSer.getUsername());
                    ucenik.setPassword(appUSer.getPassword());
                });
        return ucenici;
    }

    @Override
    @Transactional
    public List<PredmetOutputDto> getForUcenikPolozeni(Long ucenikId) {
        List<PredmetOutputDto> predmeti = new ArrayList<>();
        pohadjaRepository
                .findByUcenik_Id(ucenikId)
                .forEach(
                        pohadja -> {
                            if (pohadja.getKonacnaOcena() != null
                                    && pohadja.getKonacnaOcena() > 5) {
                                PredmetOutputDto predmet =
                                        mapper.predmetToPredmetOutputDto(pohadja.getPredmet());
                                predmet.setPohadja(new ArrayList<>());
                                predmet.getPohadja()
                                        .add(
                                                mapper.pohadjaToPohadjaOutputDto(
                                                        pohadjaRepository
                                                                .getAllByUcenik_IdAndPredmet_Id(
                                                                        ucenikId,
                                                                        predmet.getId())));
                                predmeti.add(predmet);
                            }
                        });
        return predmeti;
    }

    @Override
    @Transactional
    public List<PredmetOutputDto> getForUcenikNepolozeni(Long ucenikId) {
        List<PredmetOutputDto> predmeti = new ArrayList<>();
        pohadjaRepository
                .findByUcenik_Id(ucenikId)
                .forEach(
                        pohadja -> {
                            if (pohadja.getKonacnaOcena() == null
                                    || pohadja.getKonacnaOcena() <= 5) {
                                PredmetOutputDto predmet =
                                        mapper.predmetToPredmetOutputDto(pohadja.getPredmet());
                                predmet.setPohadja(new ArrayList<>());
                                predmet.getPohadja()
                                        .add(
                                                mapper.pohadjaToPohadjaOutputDto(
                                                        pohadjaRepository
                                                                .getAllByUcenik_IdAndPredmet_Id(
                                                                        ucenikId,
                                                                        predmet.getId())));
                                predmeti.add(predmet);
                            }
                        });
        return predmeti;
    }

    @Override
    @Transactional
    public List<PrijavaOutputDto> getPrijaveBodoviNull(Long ucenikId) {
        List<PrijavaOutputDto> prijave =
                prijavaRepository.getAllByUcenik_IdAndAndOsvojeniBodoviNot(ucenikId, null).stream()
                        .map(mapper::prijavaToPrijavaOutputDto)
                        .collect(Collectors.toList());

        return prijave;
    }

    @Override
    public List<PredmetOutputDto> getForUcenik(Long ucenikId) {
        List<PredmetOutputDto> predmeti = new ArrayList<>();
        pohadjaRepository
                .findByUcenik_Id(ucenikId)
                .forEach(
                        pohadja ->
                                predmeti.add(
                                        mapper.predmetToPredmetOutputDto(pohadja.getPredmet())));
        return predmeti;
    }

    @Override
    @Transactional
    public Boolean changePass(Long id, ChangePasswordDto changePasswordDto) {
        ApplicationUser applicationUser =
                applicationUserRepository.findAllByUserIdAndRole(id, Role.UCENIK);

        if (securityConfig
                .getaAthenticationManager()
                .authenticate(
                        new UsernamePasswordAuthenticationToken(
                                applicationUser.getUsername(),
                                changePasswordDto.getOldPassword(),
                                new ArrayList<>()))
                .isAuthenticated()) {
            applicationUser.setPassword(
                    bCryptPasswordEncoder.encode(changePasswordDto.getNewPassword()));
            applicationUserRepository.save(applicationUser);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public UcenikOutputDto add(@RequestBody UcenikInputDto ucenik) {
        if (applicationUserRepository.findByUsername(ucenik.getUsername()) == null) {

            ucenik.setId(null);
            Ucenik u = mapper.ucenikInputDtoToUcenik(ucenik);
            u = ucenikRepository.save(u);
            String password = bCryptPasswordEncoder.encode(ucenik.getPassword());
            applicationUserRepository.save(
                    new ApplicationUser(
                            null, ucenik.getUsername(), password, Role.UCENIK, u.getId()));

            UcenikOutputDto uu = mapper.ucenikToUcenikOutputDto(u);
            uu.setUsername(ucenik.getUsername());
            uu.setPassword(password);
            return uu;
        }
        List<String> errors = new ArrayList<>();
        errors.add("Username not unique");
        throw new BadRequestException(new ErrorMessage(400, "Bad Request", errors));
    }

    @Override
    @Transactional
    public UcenikOutputDto update(UcenikInputDto ucenik) {
        Ucenik u = mapper.ucenikInputDtoToUcenik(ucenik);
        u = ucenikRepository.save(u);
        String password = bCryptPasswordEncoder.encode(ucenik.getPassword());
        ApplicationUser appUser =
                applicationUserRepository.findAllByUserIdAndRole(ucenik.getId(), Role.UCENIK);
        appUser.setPassword(appUser.getPassword());
        appUser.setUsername(ucenik.getUsername());
        applicationUserRepository.save(appUser);

        UcenikOutputDto uu = mapper.ucenikToUcenikOutputDto(u);
        uu.setUsername(ucenik.getUsername());
        uu.setPassword(appUser.getPassword());

        return uu;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Ucenik nastavnik = ucenikRepository.getOne(id);
        if (applicationUserRepository.findAllByUserIdAndRole(nastavnik.getId(), Role.UCENIK)
                != null)
            applicationUserRepository.delete(
                    applicationUserRepository.findAllByUserIdAndRole(
                            nastavnik.getId(), Role.UCENIK));
        ucenikRepository.deleteById(id);
    }
}
