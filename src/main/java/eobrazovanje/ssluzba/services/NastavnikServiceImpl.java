package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.config.SecurityConfig;
import eobrazovanje.ssluzba.exception.BadRequestException;
import eobrazovanje.ssluzba.mappers.Mapper;
import eobrazovanje.ssluzba.model.Nastavnik;
import eobrazovanje.ssluzba.model.dtos.ChangePasswordDto;
import eobrazovanje.ssluzba.model.dtos.ErrorMessage;
import eobrazovanje.ssluzba.model.dtos.NastavnikInputDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikListDto;
import eobrazovanje.ssluzba.model.dtos.NastavnikOutputDto;
import eobrazovanje.ssluzba.model.dtos.PredajeOutputDto;
import eobrazovanje.ssluzba.repositories.NastavnikRepository;
import eobrazovanje.ssluzba.repositories.PredajeRepository;
import eobrazovanje.ssluzba.users.ApplicationUser;
import eobrazovanje.ssluzba.users.ApplicationUserRepository;
import eobrazovanje.ssluzba.users.Role;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NastavnikServiceImpl implements NastanikService {
    private NastavnikRepository nastavnikRepository;
    private Mapper mapper;
    private PredajeRepository predajeRepository;
    private ApplicationUserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private SecurityConfig securityConfig;

    public NastavnikServiceImpl(
            NastavnikRepository nastavnikRepository,
            Mapper mapper,
            PredajeRepository predajeRepository,
            ApplicationUserRepository applicationUserRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            SecurityConfig securityConfig) {
        this.nastavnikRepository = nastavnikRepository;
        this.mapper = mapper;
        this.predajeRepository = predajeRepository;
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.securityConfig = securityConfig;
    }

    @Override
    @Transactional
    public NastavnikListDto get() {
        List<NastavnikOutputDto> nastavnici =
                nastavnikRepository.findAll().stream()
                        .map(mapper::nastavnikToNastavnikOutputDto)
                        .collect(Collectors.toList());
        return new NastavnikListDto(nastavnici);
    }

    @Override
    @Transactional
    public List<NastavnikOutputDto> getList() {
        List<NastavnikOutputDto> nastavnici =
                nastavnikRepository.findAll().stream()
                        .map(mapper::nastavnikToNastavnikOutputDto)
                        .collect(Collectors.toList());

        nastavnici.forEach(
                nastavnik -> {
                    ApplicationUser appUSer =
                            applicationUserRepository.findAllByUserIdAndRole(
                                    nastavnik.getId(), Role.NASTAVNIK);
                    nastavnik.setUsername(appUSer.getUsername());
                });
        return nastavnici;
    }

    @Override
    public List<PredajeOutputDto> getPredmete(Long nastanikId) {
        List<PredajeOutputDto> pradaje = new ArrayList<>();
        predajeRepository
                .getAllByNastavnik_Id(nastanikId)
                .forEach(p -> pradaje.add(mapper.predajeToPredajeOutputDto(p)));
        return pradaje;
    }

    @Override
    @Transactional
    public NastavnikOutputDto get(Long id) {
        NastavnikOutputDto nastavnik =
                mapper.nastavnikToNastavnikOutputDto(nastavnikRepository.findById(id).get());
        ApplicationUser appUSer =
                applicationUserRepository.findAllByUserIdAndRole(nastavnik.getId(), Role.NASTAVNIK);
        nastavnik.setUsername(appUSer.getUsername());
        return nastavnik;
    }

    @Override
    @Transactional
    public NastavnikOutputDto add(NastavnikInputDto nastavnik) {
        if ( applicationUserRepository.findByUsername(nastavnik.getUsername()) == null) {
            nastavnik.setId(null);
            Nastavnik u = mapper.nastavnikInputDtoToNastavnik(nastavnik);
            u = nastavnikRepository.save(u);
            String password = bCryptPasswordEncoder.encode(nastavnik.getPassword());
            applicationUserRepository.save(
                    new ApplicationUser(
                            null, nastavnik.getUsername(), password, Role.NASTAVNIK, u.getId()));

            NastavnikOutputDto uu = mapper.nastavnikToNastavnikOutputDto(u);
            uu.setUsername(nastavnik.getUsername());
            return uu;
        }
        List<String> errors = new ArrayList<>();
        errors.add("Username not unique");
        throw new BadRequestException(new ErrorMessage(400, "Bad Request", errors));
    }

    @Override
    @Transactional
    public Boolean changePass(Long id, ChangePasswordDto changePasswordDto) {
        ApplicationUser applicationUser =
                applicationUserRepository.findAllByUserIdAndRole(id, Role.NASTAVNIK);

        if (securityConfig
                .getaAthenticationManager()
                .authenticate(
                        new UsernamePasswordAuthenticationToken(
                                applicationUser.getUsername(),
                                changePasswordDto.getOldPassword(),
                                new ArrayList<>()))
                .isAuthenticated()) {
            applicationUser.setPassword(
                    bCryptPasswordEncoder.encode(changePasswordDto.getNewPassword()));
            applicationUserRepository.save(applicationUser);
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public NastavnikOutputDto update(NastavnikInputDto nastavnik) {
        Nastavnik u = mapper.nastavnikInputDtoToNastavnik(nastavnik);
        u = nastavnikRepository.save(u);
        String password = bCryptPasswordEncoder.encode(nastavnik.getPassword());
        ApplicationUser appUser =
                applicationUserRepository.findAllByUserIdAndRole(nastavnik.getId(), Role.NASTAVNIK);
        appUser.setUsername(nastavnik.getUsername());
        applicationUserRepository.save(appUser);
        NastavnikOutputDto uu = mapper.nastavnikToNastavnikOutputDto(u);
        uu.setUsername(nastavnik.getUsername());
        return uu;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Nastavnik nastavnik = nastavnikRepository.getOne(id);
        if (applicationUserRepository.findAllByUserIdAndRole(nastavnik.getId(), Role.NASTAVNIK)
                != null)
            applicationUserRepository.delete(
                    applicationUserRepository.findAllByUserIdAndRole(
                            nastavnik.getId(), Role.NASTAVNIK));
        nastavnikRepository.deleteById(id);
    }
}
