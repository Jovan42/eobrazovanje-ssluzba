package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.Dokument;
import eobrazovanje.ssluzba.model.dtos.DokumentInputDto;
import eobrazovanje.ssluzba.model.dtos.DokumentOutputDto;
import java.util.List;
import org.springframework.core.io.ByteArrayResource;

public interface DokumentService
        extends GenericService<Dokument, DokumentInputDto, List<Dokument>, Long> {
    DokumentOutputDto upload(DokumentInputDto dokumentInputDto);

    ByteArrayResource download(Long id);

    List<DokumentOutputDto> getAllForUcenik(Long ucenikId);

    DokumentOutputDto getAllForUcenikAndNaslov(Long ucenikId, String naslov);

    void deleteAllByUcenikId(Long ucenikId);

}
