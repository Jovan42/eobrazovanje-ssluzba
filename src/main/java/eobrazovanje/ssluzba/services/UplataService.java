package eobrazovanje.ssluzba.services;

import eobrazovanje.ssluzba.model.dtos.UplataInputDto;
import eobrazovanje.ssluzba.model.dtos.UplataListDto;
import eobrazovanje.ssluzba.model.dtos.UplataOutputDto;
import java.util.List;

public interface UplataService
        extends GenericService<UplataOutputDto, UplataInputDto, UplataListDto, Long> {
    List<UplataOutputDto> getList();
}
